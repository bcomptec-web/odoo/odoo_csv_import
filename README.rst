Odoo CSV Import Export Library
==============================
This library provides tools to easily and quickly import data into Odoo or export data from Odoo using CSV file. 
It also provide a framework to manipulate date from csv.

Requirements
--------------
* openerp-client-lib

How To
------
- `How to Import large data into Odoo (PDF)
  <https://www.odoo.com/slides/slide/how-to-import-large-complex-data-into-odoo-455/pdf_content>`_
- `Odoo Experience 2018 - Successful Import of Big Data with an Efficient Tool (Video)
  <https://www.youtube.com/watch?v=FbW0Wu5buY8>`_
- Example usage: :code:`odoo_csv_import/odoo_import_thread.py -s$'\t' --quote="\"" --config "odoo_csv_import/connection.conf" --model=crm.lead --file "$csvfile"`